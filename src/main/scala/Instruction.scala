import Direction._
/** An instruction for the robot
  *
  *  @constructor create a new Instruction with a direction and a number of steps from a case class.
  *  @param direction the direction on the compass the robot should head
  *  @param steps the number of steps that the robot should take in said direction
  */
case class Instruction(direction: Option[Direction], steps: Int)
