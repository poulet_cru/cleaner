import org.scalatest.FunSuite
import org.scalatest.Matchers._

import scala.io.Source

class MainTest extends FunSuite {

  test("getCleanedPositionsFromInput() - first basic example"){
    val inputs = Source.fromFile("src/main/resources/examples/basic1.txt").getLines.toList
    Main.getCleanedPositionsFromInput(inputs) should be (9)
  }

  test("getCleanedPositionsFromInput() - second basic example"){
    val inputs = Source.fromFile("src/main/resources/examples/basic2.txt").getLines.toList

    Main.getCleanedPositionsFromInput(inputs) should be (4)
  }

  test("getCleanedPositionsFromInput() should not execute all the instructions if the number of instructions is " +
    "superior to the number of instructions the robot was expecting"){
    val inputs = Source.fromFile("src/main/resources/examples/toManyInstructions.txt").getLines.toList
    Main.getCleanedPositionsFromInput(inputs) should be (6)
  }

  test("getCleanedPositionsFromInput() should return the right number of cleaned coordinates when we hit the grid"){
    val inputs = Source.fromFile("src/main/resources/examples/hittingTheGrid.txt").getLines.toList
    Main.getCleanedPositionsFromInput(inputs) should be (13)
  }

}
