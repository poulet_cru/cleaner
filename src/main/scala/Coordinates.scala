/** A coordinates in the grid
  *
  *  @constructor create a new coordinates with a 'x' and a 'y' from a case class.
  *  @param x the position on the x-axis
  *  @param y the position on the y-axis
  */
case class Coordinates(x: Int, y: Long)

object Coordinates {
  /** Maximum ranges for the x and y axis **/
  val maxX, maxY = 100000
  /** Minimum ranges for the x and y axis **/
  val minX, minY = -100000
}
