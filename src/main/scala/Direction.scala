/** Enumeration for the direction to follow in the instruction.
  * Possible value :
  *  - E = East
  *  - W = West
  *  - S = South
  *  - N = North
  */
object Direction extends Enumeration {
  type Direction = Value

  val E, W, S, N = Value

  /** Creates an Option[Direction.Value] from a string
    *
    *  @param s the input string
    *  @return an Option[Direction.Value] that matches the input string
    */
  def fromString(s: String): Option[Value] = values.find(_.toString == s)
}