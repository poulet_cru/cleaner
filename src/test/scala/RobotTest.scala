import org.scalatest.FunSuite
import org.scalatest.Matchers._

import scala.collection.mutable

class RobotTest extends FunSuite {

  test("default Robot should have empty set") {
    val robot = Robot(Coordinates(0, 0))
    robot.cleaned.size should be(0)
  }

  test("doNotMove() should return None") {
    Robot.doNotMove shouldBe a[Option[_]]
    Robot.doNotMove should be(None)
  }

  test("moveSouth() should decrease the coordinates.y by the amounts of steps - starting at the origin") {
    val init = Coordinates(0, 0)
    val steps = 10
    val result = Robot.moveSouth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(0, -10))
  }

  test("moveSouth() should decrease the coordinates.y by the amounts of steps - starting at random point on the grid") {
    val init = Coordinates(-90000, -40000)
    val steps = 20000
    val result = Robot.moveSouth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-90000, -60000))
  }

  test("moveSouth() should return a coordinate if we just hit the bounds of the grid") {
    val init = Coordinates(-90000, -90000)
    val steps = 10000
    val result = Robot.moveSouth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-90000, -100000))
  }

  test("moveSouth() should return None if we try to go beyond the bounds of the grid") {
    val init = Coordinates(-90000, -90000)
    val steps = 10001
    val result = Robot.moveSouth(init, steps)
    result shouldBe a[Option[_]]
    result should be(None)
  }

  test("moveNorth() should increase the coordinates.y by the amounts of steps - starting at the origin") {
    val init = Coordinates(0, 0)
    val steps = 10
    val result = Robot.moveNorth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(0, 10))
  }

  test("moveNorth() should increase the coordinates.y by the amounts of steps - starting at random point on the grid") {
    val init = Coordinates(-90000, -40000)
    val steps = 20000
    val result = Robot.moveNorth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-90000, -20000))
  }

  test("moveNorth() should return a coordinate if we just hit the bounds of the grid") {
    val init = Coordinates(-90000, 90000)
    val steps = 10000
    val result = Robot.moveNorth(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-90000, 100000))
  }

  test("moveNorth() should return None if we try to go beyond the bounds of the grid") {
    val init = Coordinates(-90000, 90000)
    val steps = 10001
    val result = Robot.moveNorth(init, steps)
    result shouldBe a[Option[_]]
    result should be(None)
  }

  test("moveEast() should increase the coordinates.x by the amounts of steps - starting at the origin") {
    val init = Coordinates(0, 0)
    val steps = 10
    val result = Robot.moveEast(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(10, 0))
  }

  test("moveEast() should increase the coordinates.x by the amounts of steps - starting at random point on the grid") {
    val init = Coordinates(-90000, -40000)
    val steps = 20000
    val result = Robot.moveEast(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-70000, -40000))
  }

  test("moveEast() should return a coordinate if we just hit the bounds of the grid") {
    val init = Coordinates(90000, 90000)
    val steps = 10000
    val result = Robot.moveEast(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(100000, 90000))
  }

  test("moveEast() should return None if we try to go beyond the bounds of the grid") {
    val init = Coordinates(90000, 90000)
    val steps = 10001
    val result = Robot.moveEast(init, steps)
    result shouldBe a[Option[_]]
    result should be(None)
  }

  test("moveWest() should decrease the coordinates.x by the amounts of steps - starting at the origin") {
    val init = Coordinates(0, 0)
    val steps = 10
    val result = Robot.moveWest(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-10, 0))
  }

  test("moveWest() should decrease the coordinates.x by the amounts of steps - starting at random point on the grid") {
    val init = Coordinates(-70000, -40000)
    val steps = 20000
    val result = Robot.moveWest(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-90000, -40000))
  }

  test("moveWest() should return a coordinate if we just hit the bounds of the grid") {
    val init = Coordinates(-90000, 90000)
    val steps = 10000
    val result = Robot.moveWest(init, steps)
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Coordinates(-100000, 90000))
  }

  test("moveWest() should return None if we try to go beyond the bounds of the grid") {
    val init = Coordinates(-90000, 90000)
    val steps = 10001
    val result = Robot.moveWest(init, steps)
    result shouldBe a[Option[_]]
    result should be(None)
  }

  test("clean() should return the correct seq of coordinates starting from the origin and going East") {
    val init = Coordinates(0, 0)
    val instruction = Instruction(Some(Direction.E), 4)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(1, 0), Coordinates(2, 0), Coordinates(3, 0), Coordinates(4, 0)))
  }

  test("clean() should return the correct seq of coordinates starting at random and going West") {
    val init = Coordinates(-10, 5)
    val instruction = Instruction(Some(Direction.W), 3)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(-11, 5), Coordinates(-12, 5), Coordinates(-13, 5)))
  }

  test("clean() should return the correct seq of coordinates starting at random and going North") {
    val init = Coordinates(-10, 5)
    val instruction = Instruction(Some(Direction.N), 4)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(-10, 6), Coordinates(-10, 7), Coordinates(-10, 8), Coordinates(-10, 9)))
  }

  test("clean() should return the correct seq of coordinates starting at random and going South") {
    val init = Coordinates(-10, 5)
    val instruction = Instruction(Some(Direction.S), 3)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(-10, 4), Coordinates(-10, 3), Coordinates(-10, 2)))
  }

  test("clean() should return the correct seq of coordinates going South except the ones beyond the grid") {
    val init = Coordinates(0, -99997)
    val instruction = Instruction(Some(Direction.S), 7)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(0, -99998), Coordinates(0, -99999), Coordinates(0, -100000)))
  }

  test("clean() should return the correct seq of coordinates going North except the ones beyond the grid") {
    val init = Coordinates(0, 99997)
    val instruction = Instruction(Some(Direction.N), 10)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(0, 99998), Coordinates(0, 99999), Coordinates(0, 100000)))
  }

  test("clean() should return the correct seq of coordinates going West except the ones beyond the grid") {
    val init = Coordinates(-99997, 0)
    val instruction = Instruction(Some(Direction.W), 100)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(-99998, 0), Coordinates(-99999, 0), Coordinates(-100000, 0)))
  }

  test("clean() should return the correct seq of coordinates going East except the ones beyond the grid") {
    val init = Coordinates(99997, 0)
    val instruction = Instruction(Some(Direction.E), 10)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq(Coordinates(99998, 0), Coordinates(99999, 0), Coordinates(100000, 0)))
  }

  test("clean() should return an empty seq for an instruction that doesn't have a valid direction") {
    val init = Coordinates(0, 0)
    val instruction = Instruction(None, 10)
    val result = Robot.clean(instruction, init)
    result shouldBe a[Seq[_]]
    result should be(Seq())
  }

  test("move() should return the correct set of coordinates that have been cleaned given the instructions") {
    val init = Coordinates(0, 0)
    val instructions = List(
      Instruction(Some(Direction.E), 2),
      Instruction(Some(Direction.N), 1),
      Instruction(Some(Direction.W), 1)
    )
    val result = Robot.move(instructions, Set(), init)
    result shouldBe a[Set[_]]
    result.size should be(4)
    result should be(
      Set(
        Coordinates(1, 0),
        Coordinates(2, 0),
        Coordinates(2, 1),
        Coordinates(1, 1)
      )
    )
  }

  test("move() should return the correct set of coordinates that have been cleaned given the instructions " +
    "and a non empty Set") {
    val init = Coordinates(0, 0)
    val instructions = List(
      Instruction(Some(Direction.E), 2),
      Instruction(Some(Direction.N), 1),
      Instruction(Some(Direction.W), 1)
    )
    val result = Robot.move(instructions, Set(Coordinates(0, 0), Coordinates(-1, 0)), init)
    result shouldBe a[Set[_]]
    result.size should be(6)
    result should be(
      Set(
        Coordinates(-1, 0),
        Coordinates(0, 0),
        Coordinates(1, 0),
        Coordinates(2, 0),
        Coordinates(2, 1),
        Coordinates(1, 1)
      )
    )
  }

  test("move() should return the correct set of coordinates that have been cleaned given the instructions " +
    "with overlapping coordinates") {
    val init = Coordinates(0, 0)
    val instructions = List(
      Instruction(Some(Direction.E), 3),
      Instruction(Some(Direction.W), 2),
      Instruction(Some(Direction.N), 1)
    )
    val result = Robot.move(instructions, Set(Coordinates(1, 0), Coordinates(0, 0)), init)
    result shouldBe a[Set[_]]
    result.size should be(5)
    result should be(
      Set(
        Coordinates(1, 0),
        Coordinates(0, 0),
        Coordinates(2, 0),
        Coordinates(3, 0),
        Coordinates(1, 1)
      )
    )
  }

  test("move() should return the correct set of coordinates that have been cleaned given the instructions " +
    "without going beyond the grid") {
    val init = Coordinates(99998, 0)
    val instructions = List(
      Instruction(Some(Direction.N), 3),
      Instruction(Some(Direction.E), 100),
      Instruction(Some(Direction.S), 1)
    )
    val result = Robot.move(instructions, Set(init), init)
    result shouldBe a[Set[_]]
    result.size should be(7)
    result should be(
      Set(
        Coordinates(99998, 0),
        Coordinates(99998, 1),
        Coordinates(99998, 2),
        Coordinates(99998, 3),
        Coordinates(99999, 3),
        Coordinates(100000, 3),
        Coordinates(100000, 2),
      )
    )
  }

  test("getNumberOfCleanedPositions() should return the correct size of the set") {
    Robot(Coordinates(0, 0)).getNumberOfUniqueCleanedPositions should be(0)
    Robot(Coordinates(0, 0), mutable.Set(Coordinates(1, 1))).getNumberOfUniqueCleanedPositions should be(1)
    Robot(Coordinates(0, 0), mutable.Set(Coordinates(1, 1), Coordinates(1, 3))).getNumberOfUniqueCleanedPositions should be(2)

  }

  test("execute() should update the robot's cleaned set correctly given the instructions") {
    val robot = Robot(Coordinates(5, 5))
    val instructions = List(
      Instruction(Some(Direction.E), 3),
      Instruction(Some(Direction.W), 2),
      Instruction(Some(Direction.N), 1),
      Instruction(Some(Direction.N), 2)
    )
    robot.execute(instructions)
    val result = robot.cleaned
    result shouldBe a[mutable.Set[_]]
    robot.getNumberOfUniqueCleanedPositions should be(7)
    result should be(
      mutable.Set(
        Coordinates(5, 5),
        Coordinates(6, 5),
        Coordinates(7, 5),
        Coordinates(8, 5),
        Coordinates(6, 6),
        Coordinates(6, 7),
        Coordinates(6, 8),

      )
    )
  }

  test("execute() should update the robot's set correctly given the instructions without going beyond the grid") {
    val robot = Robot(Coordinates(99998, 5))
    val instructions = List(
      Instruction(Some(Direction.S), 3),
      Instruction(Some(Direction.E), 200),
      Instruction(Some(Direction.N), 1)
    )
    robot.execute(instructions)
    val result = robot.cleaned
    result shouldBe a[mutable.Set[_]]
    robot.getNumberOfUniqueCleanedPositions should be(7)
    result should be(
      mutable.Set(
        Coordinates(99998, 5),
        Coordinates(99998, 4),
        Coordinates(99998, 3),
        Coordinates(99998, 2),
        Coordinates(99999, 2),
        Coordinates(100000, 2),
        Coordinates(100000, 3),
      )
    )
  }

  test("execute() should only add the starting coordinates to the set if the instructions list is empty") {
    val robot = Robot(Coordinates(5, 5))
    val instructions = List()
    robot.execute(instructions)
    val result = robot.cleaned
    result shouldBe a[mutable.Set[_]]
    robot.getNumberOfUniqueCleanedPositions should be(1)
    result should be(
      mutable.Set(
        Coordinates(5, 5)
      )
    )
  }

}
