# Awesome Robot Cleaner

An automatic cleaning robot that cleans the office at night.

## Initial Configuration

- sbt v1.0.3+
- scala v2.12.6+

## Cloning

```
git clone https://github.com/poulet_cru/cleaner.git
cd cleaner/ 
```

## Building


```shell
sbt assembly
```

## Using

The assembly will generate a jar located in `./target/scala-2.12/`.    
To launch the robot, execute the following command :
```shell
cat ${inputFilePath} | scala target/scala-2.12/cleaner-assembly-0.1.jar Main
```

## Testing

```
sbt test
```

## Code Coverage

### Generate Coverage Data

```
sbt clean coverage test
sbt coverageReport
```

### Opening Coverage
- macOs :
```
open target/scala-2.12/scoverage-report/index.html
```

- Linux : 
```
xdg-open target/scala-2.12/scoverage-report/index.html
```
