import scala.annotation.tailrec
import scala.collection.mutable

/** A Robot Cleaner
  *
  * @constructor create a new robot cleaner from a case class with an initial position and a mutable set containing
  *              the unique places it has cleaned.
  * @param init    the initial coordinates of the robot
  * @param cleaned the set of all (unique) places it has cleaned
  */
case class Robot(init: Coordinates, cleaned: mutable.Set[Coordinates] = mutable.Set()) {
  /**
    * We chose to use a mutable set instead of an immutable one. Even if the functional programming is a paradigm that
    * encourages us to use thread-safe code (e.g with immutability), the "stateful" nature of the current problem
    * allows us to use a mutable set for a better structure and understanding of the program.
    */

  /** Execute a list of instructions for cleaning the place
    *
    * @param instructions the list of Instruction the robot should follow for cleaning the place
    * @return an Option[Direction.Value] that matches the input string
    */
  def execute(instructions: List[Instruction]): Unit = {
    /*
    Initialize the move function, which is tail recursive, with a cleaned set containing the starting position
     */
    this.cleaned ++= Robot.move(instructions, Set(this.init), this.init)
  }

  /** Return the number of unique cleaned position
    *
    * @return the size of the cleaned set (i.e the number of unique cleaned positions)
    */
  def getNumberOfUniqueCleanedPositions = this.cleaned.size

}

object Robot {

  /** Return the set of all coordinates the robot has cleaned given some instructions, the past cleaned coordinates and
    * the robot's starting coordinates. This function is tail recursive.
    *
    * @param toDo    a list of instructions the robot should follow
    * @param cleaned a set containing all the previous coordinates the robot has cleaned
    * @param init    the robot's starting coordinates
    * @return the set of all coordinates the robot has cleaned
    */
  @tailrec
  def move(toDo: List[Instruction], cleaned: Set[Coordinates], init: Coordinates): Set[Coordinates] = {
    toDo match {
      // case where the list contains at least one instruction
      case instruction :: restToDo =>
        // get the new cleaned position from the first instruction of the list and the actual robot's position
        val newCleaned = clean(instruction, init)
        /* recursive call with the instructions (that remain to be done) list, a new set of cleaned positions containing
        the previous cleaned positions and the new ones, and the current position of the robot (i.e the position where
        it stopped).
        */
        move(restToDo, cleaned ++ newCleaned.toSet, newCleaned.last)
      // case where the list is empty. E.g equals to Nil. Return the set of positions it has cleaned
      case _ => cleaned
    }
  }

  /** Return a sequence of coordinates the robot has cleaned given a single instruction and a starting coordinates.
    * The robot can't be sent outside the bounds of the plane.
    *
    * @param instruction a single instruction the robot has to follow for cleaning
    * @param init        the robot's starting coordinates
    * @return a sequence of all coordinates the robot has cleaned
    */
  def clean(instruction: Instruction, init: Coordinates): Seq[Coordinates] = {
    val steps = instruction.steps

    /* get the right moving function depending on the direction on the compass. If the direction is unknown, the robot
    won't move.
    */
    def moveInDirectionFunc(init: Coordinates, i: Int): Option[Coordinates] = instruction.direction match {
      case Some(Direction.E) => moveEast(init, i)
      case Some(Direction.W) => moveWest(init, i)
      case Some(Direction.N) => moveNorth(init, i)
      case Some(Direction.S) => moveSouth(init, i)
      case _ => doNotMove
    }
    /*
     iterate over all the steps to create a seq of cleaned positions from the correct moving function. The flatMap
     allows us to eliminate the coordinates that are beyond the grid (returned as None by the moving function).
      */
    (1 to steps).flatMap(i => moveInDirectionFunc(init, i))
  }

  /** Return a new Option[coordinates] after moving from an initial position for a certain amount of steps to the east.
    * This function returns None if the computed coordinates is off grid.
    *
    * @param init  the robot's starting coordinates
    * @param steps the number of steps the robot should go to the east
    * @return an Option[Coordinates]:
    *         - Some(Coordinates) if the coordinates is correct,
    *         - None if the coordinates is off grid.
    */
  def moveEast(init: Coordinates, steps: Int): Option[Coordinates] = {
    if (init.x + steps <= Coordinates.maxX) Option(Coordinates(init.x + steps, init.y)) else None
  }

  /** Return a new Option[coordinates] after moving from an initial position for a certain amount of steps to the west.
    * This function returns None if the computed coordinates is off grid.
    *
    * @param init  the robot's starting coordinates
    * @param steps the number of steps the robot should go to the west
    * @return an Option[Coordinates]:
    *         - Some(Coordinates) if the coordinates is correct,
    *         - None if the coordinates is off grid.
    */
  def moveWest(init: Coordinates, steps: Int): Option[Coordinates] = {
    if (init.x - steps >= Coordinates.minX) Option(Coordinates(init.x - steps, init.y)) else None
  }

  /** Return a new Option[coordinates] after moving from an initial position for a certain amount of steps to the north.
    * This function returns None if the computed coordinates is off grid.
    *
    * @param init  the robot's starting coordinates
    * @param steps the number of steps the robot should go to the north
    * @return an Option[Coordinates]:
    *         - Some(Coordinates) if the coordinates is correct,
    *         - None if the coordinates is off grid.
    */
  def moveNorth(init: Coordinates, steps: Int): Option[Coordinates] = {
    if (init.y + steps <= Coordinates.maxY) Option(Coordinates(init.x, init.y + steps)) else None
  }

  /** Return a new Option[coordinates] after moving from an initial position for a certain amount of steps to the south.
    * This function returns None if the computed coordinates is off grid.
    *
    * @param init  the robot's starting coordinates
    * @param steps the number of steps the robot should go to the south
    * @return an Option[Coordinates]:
    *         - Some(Coordinates) if the coordinates is correct,
    *         - None if the coordinates is off grid.
    */
  def moveSouth(init: Coordinates, steps: Int): Option[Coordinates] = {
    if (init.y - steps >= Coordinates.minY) Option(Coordinates(init.x, init.y - steps)) else None
  }

  /** Return None. The robot should not move.
    *
    * @return and Option[Coordinates] which will always be None
    */
  def doNotMove: Option[Coordinates] = None

}

