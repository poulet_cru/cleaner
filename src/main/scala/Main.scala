import scala.io.Source

/** Main object that parse the input file, launch the robot and output the result */
object Main {

  // $COVERAGE-OFF$Disable the coverage on the main function
  /** Main function: Parse the input, launch the robot and output the number of unique cleaned positions by the robot.
    *
    * @param args an array of optional inputs to the program. One could specify the path to the inputFile as the first
    *             element of the array.
    */
  def main(args: Array[String]): Unit = {
    val inputs = io.Source.stdin.getLines.toList
    val cleanedPositions = getCleanedPositionsFromInput(inputs)
    println(s"=> Cleaned: $cleanedPositions")
  }
  // $COVERAGE-ON$

  /** Parse the inputs. Start the robot. Return the number of unique cleaned positions by the robot.
    *
    * @param inputs the inputs line as a list
    * @return the number of unique cleaned positions by the robot.
    */
  def getCleanedPositionsFromInput(inputs: List[String]): Int = {
    // separate the first and second lines from the rest of the lines of the input file
    val (numberOfCommandsInput :: coordinates :: instructionsList) = inputs
    // the number of commands 'n' the robot should expect is in the range 0 <= n <= 10000
    val numberOfCommands = Math.min(Math.max(0, numberOfCommandsInput.toInt), 10000)
    val instructions = instructionsList
      .map(instruction => {
        val instructionSplit = instruction.split(" ")
        Instruction(
          Direction.fromString(instructionSplit.head),
          instructionSplit.last.toInt
        )
      }
      )
      // we limit the number of instructions to the 'numberOfCommands' first instructions.
      .take(numberOfCommands)
    val coordinatesSplit = coordinates.split(" ").map(_.toInt)
    // init the robot with the starting coordinates specified in the input file
    val robot = Robot(
      Coordinates(
        coordinatesSplit.head,
        coordinatesSplit.last
      )
    )
    // launch the robot with the list of instructions it should follow
    robot.execute(instructions)
    // return the number of unique cleaned positions by the robot
    robot.getNumberOfUniqueCleanedPositions
  }

}
