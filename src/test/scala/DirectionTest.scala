import org.scalatest.FunSuite
import org.scalatest.Matchers._

class DirectionTest extends FunSuite {

  test("fromString() should return an option of Direction.W") {
    val result = Direction.fromString("W")
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Direction.W)
  }

  test("fromString() should return an option of Direction.E") {
    val result = Direction.fromString("E")
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Direction.E)
  }

  test("fromString() should return an option of Direction.N") {
    val result = Direction.fromString("N")
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Direction.N)
  }

  test("fromString() should return an option of Direction.S") {
    val result = Direction.fromString("S")
    result shouldBe a[Option[_]]
    result should be('defined)
    result.get should be(Direction.S)
  }

  test("fromString() should return None if the input string doesn't match any direction") {
    val result = Direction.fromString("Doesn't exist")
    result shouldBe a[Option[_]]
    result should not be ('defined)
    result should be(None)
  }

}
